//Опишіть своїми словами що таке Document Object Model (DOM)
//Об'єктна модель документа. Із завантаженого з сервера вебсайту браузер формує DOM у вигляді дерева тегів призначення якого є управління
//за допомогою js можливостями, зовнішнім виглядом і т.д. сайту
//Яка різниця між властивостями HTML-елементів innerHTML та innerText?
//innerHTML показує текстову інформ вкл і розмітку HTML innerText показує весь текстовий зміст який не відноститься до синтаксису HTML
//Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
//кращий спосіб querySelectorAll, можна використати любий селектор. querySelector getElementsByID getElementsByName getElementsByTagName  getElementsByClassName


const itemsList = document.querySelectorAll("p");
console.log(itemsList);

itemsList.forEach( function (item) {
item.style.backgroundColor = '#ff0000';
});

let elem = document.getElementById('optionsList');
console.log(elem);
console.log(elem.parentElement);
let node = document.getElementById('optionsList').childNodes;
console.log(node);
for (let elem of node) {
    console.log(elem.nodeName+" " + elem.nodeType);}

let testParagraph = document.getElementById("testParagraph");
testParagraph.innerText = "This is a paragraph";
console.log(testParagraph);

let elemMainHeader=document.querySelector(".main-header").children;
console.log(elemMainHeader);

for (let elem of elemMainHeader) {
    elem.classList.add("nav-item")};

let elemSecTitle=document.querySelector(".section-title");
console.log(elemSecTitle);

for (let elem of elemSecTitle) {
    elem.classList.remove("section-title")};
elemSecTitle=document.querySelector(".section-title");
console.log(elemSecTitle);

